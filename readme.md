Taks Covered
========

* Rest Framework Routers
* Rest Framework ViewSets - GenericViewSet, ModelViewSet
    * Rest Framework Mixins - DestroyModelMixin, UpdateModelMixin, ListModelMixin, CreateModelMixin, RetrieveModelMixin
* Rest framework functions - list, create, update, partial_update, retrieve and destroy
* Rest Framework Serializers - ModelSerializer, serializer fields, Nested Serializer
* Models - makemigrations, migrate
* User - django.contrib.auth.models, default authorization, default permissions

Functionality Added
============

* Basic User Auth
* Is Authenticated Permissions
* Create posts
* View Posts filtered by author
* Comment on a Post
* View Post with all its comments

To Do
====

A logged in user viewing an authors post is able to like it.

* Likes
    * Design Model To Store Likes
        * Hint: Likes contain foreign key to user and post
    * Create Serializer, ViewSet for Likes
    * Register router for Likes
    * Modify PostSerializer to show count of likes
        * Hint: return post.likes.count() using [SerializerMethodField](http://www.django-rest-framework.org/api-guide/fields/#serializermethodfield)

Example:
User 1 is viewing post 2
to like it, he requests
  POST /likes/
  post: 2
