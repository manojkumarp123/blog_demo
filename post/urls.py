from rest_framework.routers import SimpleRouter
from .views import PostViewSet, CommentViewSet
# from django.conf.urls import url

router = SimpleRouter()
router.register('posts', PostViewSet)
router.register('comments', CommentViewSet)


urlpatterns = router.urls

# urlpatterns =[
#     url(r"^posts/", PostViewSet.as_view({"delete": "list"}))
# ]
