from rest_framework.viewsets import ModelViewSet
from .models import Post, Comment
from .serializers import PostSerializer, CommentSerializer
# from rest_framework.mixins import (
#     DestroyModelMixin, UpdateModelMixin,
#     ListModelMixin, CreateModelMixin, RetrieveModelMixin)
from .permissions import IsOwnerOrReadOnly


class PostViewSet(ModelViewSet):
    serializer_class = PostSerializer
    queryset = Post.objects.all()
    filter_fields = ('author', )
    permission_classes = (IsOwnerOrReadOnly, )


class CommentViewSet(ModelViewSet):
    serializer_class = CommentSerializer
    queryset = Comment.objects.all()
