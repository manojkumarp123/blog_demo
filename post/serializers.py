from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from .models import Post, Comment
from django.contrib.auth.models import User



class CommentSerializer(ModelSerializer):
    class Meta:
        model = Comment
        fields = "__all__"


class PostSerializer(ModelSerializer):
    comments = CommentSerializer(many=True, read_only=True)
    author = serializers.PrimaryKeyRelatedField(
        read_only=True,
        default=serializers.CurrentUserDefault())

    class Meta:
        model = Post
        fields = "__all__"
