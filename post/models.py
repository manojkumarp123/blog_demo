from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    author = models.ForeignKey(User)


class Comment(models.Model):
    body = models.CharField(max_length=200)
    parent_post = models.ForeignKey(Post, related_name="comments")
